<html>
    <head>
        <meta charset="UTF-8">
        <title>LWrame 0.1b</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" 
              href="bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" type="text/css" href="wireframes/font/flaticon.css"> 
    </head>
    <body>
        <script type="text/javascript" src="/materialize/js/materialize.min.js"></script>

        <nav>
            <div class="nav-wrapper  grey darken-4">
                <a href="index.php" class="brand-logo"><img src="imag/Logo.png" alt="Logo" style="width:200px;" ></a>

                <ul class="right hide-on-med-and-down">
                    <li><a href=""><i class="material-icons">search</i></a></li>
                    <li><a href=""><i class="material-icons">view_module</i></a></li>
                    <li><a href=""><i class="material-icons">refresh</i></a></li>
                    <li><a href=""><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </nav>
        <div class="row red darken-3 ">
            <div class="col s4 ">
                <ul class="collection ">
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-1 circle red darken-3"></i>
                        <span class="title">Texto con Imagen</span>
                        <p>Imagen a la Derecha <br>

                        </p>
                        <a  href="vista1_ImgTextDer.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light deep-orange ">Insertar</button>
                        </a>
                    </li>
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-24 circle red darken-3  " ></i>
                        <span class="title">Texto con Imagen</span>
                        <p>Imagen a la Izquierda <br>
                        </p>
                        <a href="vista2_ImgTextIzq.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light deep-orange">Insertar</button>
                        </a>
                    </li>
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-20 circle red darken-3  "></i>
                        <span class="title">Titulo con Imagen y texto</span>
                        <p>Imagen a la Derecha <br>
                        </p>
                        <a href="vista3_TitImgTextDer.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light deep-orange">Insertar</button>
                        </a>
                    </li>
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-19 circle red darken-3  "></i>
                        <span class="title">Titulo con Imagen y texto</span>
                        <p>Imagen a la Izquierda <br>
                        </p>
                        <a href="vista4_TitImgTextIzq.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light  deep-orange">Insertar</button>
                        </a>
                    </li>
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-13 circle red darken-3  "></i>
                        <span class="title">Titulo con 2 Imagen y texto</span>
                        <p>Imagenes a la Derecha <br>
                        </p>
                        <a href="vista5_2ImgText.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light  deep-orange">Insertar</button>
                        </a>
                    </li>
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-42 circle red darken-3  "></i>
                        <span class="title">Other</span>
                        <p>Casillas <br>
                        </p>
                        <a href="vista6_Other.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light  deep-orange">Insertar</button>
                        </a>
                    </li>
                    <li class="collection-item avatar">
                        <i class="flaticon-browser-47 circle red darken-3  "></i>
                        <span class="title">Other</span>
                        <p>Casillas <br>
                        </p>
                        <a href="vista7_Other.php" class="secondary-content">
                            <button type="button" class="btn btn-outline-light  deep-orange">Insertar</button>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col s4 center grey lighten-2 ">
                <img src="imag/vista5.png" alt="" >
            </div>
        </div>
        <h6 class="center">
            ©2018 vL1c mas proyectos <a href="https://gitlab.com/vL1c">GitLab</a><br>  
            Prototipo LWRAME Hackaton 2018 LAAREN <br>
            Font generated by <a href="http://www.flaticon.com">flaticon.com</a>
        </h6>
    </body>
</html>